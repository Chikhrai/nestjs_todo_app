import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { HttpExceptionFilter } from './filters/http_exception.filter';
import { path } from 'app-root-path';
import { join } from 'path';

const { version } = require(join(path, 'package.json'));

const PORT = process.env.PORT || 3000;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: [process.env.NODE_ENV === 'production' ? 'error' : 'log'],
  });

  app.useGlobalFilters(new HttpExceptionFilter());

  const document = SwaggerModule.createDocument(
    app,
    new DocumentBuilder()
      .setTitle('TODO API')
      .setDescription(
        `<h2>
          Базовый CRUD для оттачивания навыков студентов компьютерной школы 
          <a href="https://hillel.it" style="font-size: inherit" target="_blank">
            Hillel</a> умения работы с API =)
        </h2>`,
      )
      .setVersion(version)
      .addTag('todo')
      .addSecurity('bearer', {
        type: 'http',
        scheme: 'bearer',
        description: 'JWT Authorization',
        bearerFormat: 'Bearer ',
      })
      .build(),
  );
  SwaggerModule.setup('/', app, document);

  await app.listen(PORT, '0.0.0.0');
  console.log(`App started on port ${PORT}, ${process.env.NODE_ENV}`);
}
bootstrap();
