import { MinLength, Min, Max, IsInt } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class createTodoDto {
  @ApiProperty({
    type: 'string',
    minLength: 1,
    description: 'Task text',
    example: 'Buy license for WebStorm =)',
  })
  @MinLength(1)
  value: string;

  @ApiProperty({
    type: 'integer',
    minimum: 0,
    maximum: 3,
    default: 1,
    example: 1,
  })
  @Min(0)
  @Max(3)
  @IsInt()
  priority: number;
}
