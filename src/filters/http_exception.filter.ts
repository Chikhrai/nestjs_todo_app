import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  BadRequestException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';
import { ValidationError } from 'class-validator';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    const is_http_exception = exception instanceof HttpException;
    const status = is_http_exception
      ? exception.getStatus()
      : exception instanceof EntityNotFoundError
      ? HttpStatus.NOT_FOUND
      : HttpStatus.INTERNAL_SERVER_ERROR;

    const message =
      (exception.message && exception.message.error) ||
      // Not show EntityNotFoundError message from DB
      (status !== HttpStatus.NOT_FOUND && exception.message) ||
      exception.name;

    // Handle validation errors
    const error_messages =
      (<any>exception).response && (<any>exception).response.message;
    const validation_errors =
      (Array.isArray(error_messages) &&
        error_messages[0] instanceof ValidationError &&
        error_messages) ||
      [];

    response.status(status).json({
      statusCode: status,
      message,
      timestamp: new Date().toISOString(),
      path: request.url,
      ...(validation_errors.length ? { validation_errors } : {}),
    });
  }
}
