const ignore_watch = ['node_modules'];

const env = {
  NODE_ENV: 'production',
};

const env_dev = {
  NODE_ENV: 'dev',
};

module.exports = {
  apps: [
    {
      name: 'todo-app',
      script: './dist/main.js',
      log_date_format: 'YYYY-MM-DD HH:mm:ss',
      log_file: '~/.pm2/logs/todo-out.log',
      ignore_watch,
      env,
      env_dev,
    },
  ],
};
