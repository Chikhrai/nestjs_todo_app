const { homedir } = require('os');
const { join } = require('path');
const HOME = homedir();

module.exports = {
  type: 'sqlite',
  database: join(HOME, 'todo_data', 'db.sqlite'),
  entities: ['./dist/**/*.entity.js'],
  migrations: ['./dist/migrations/*.js'],
  synchronize: true,
  logging: 'all',
};
